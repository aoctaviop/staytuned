//
//  FilesDownloader.swift
//  StayTuned
//
//  Created by Andrés on 5/15/17.
//  Copyright © 2017 StayTuned. All rights reserved.
//

import UIKit
import Alamofire

class FilesDownloader: NSObject {
    
    let baseURL: String = "http://com.staytunedapp.danielmunnoz.com/files/audio/"
    
    // MARK: - Properties
    
    private static var sharedFilesDownloader: FilesDownloader = {
        let networkManager = FilesDownloader()
        return networkManager
    }()
    
    // MARK: - Accessors
    
    class func shared() -> FilesDownloader {
        return sharedFilesDownloader
    }
    
    func downloadFile(fileName: String, progressUpdate: @escaping (_ currenProgress: Double) -> Void, completion: @escaping (_ fileURL: URL) -> Void) {
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            let fileURL = documentsURL.appendingPathComponent(fileName)
            
            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
        }
        
        Alamofire.download(baseURL.appending(fileName), to: destination)
            .response { response in
                print(response.destinationURL!)
                completion(response.destinationURL!)
            }
            .downloadProgress { progress in
                print("Download Progress: \(progress.fractionCompleted)")
                progressUpdate(progress.fractionCompleted)
        }
    }
    
    func cancelDownloads() {
        let sessionManager = Alamofire.SessionManager.default
        sessionManager.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in dataTasks.forEach { $0.cancel() }
            uploadTasks.forEach { $0.cancel() }
            downloadTasks.forEach { $0.cancel() }
        }
    }
}


