//
//  ViewController.swift
//  StayTuned
//
//  Created by Andrés on 5/8/17.
//  Copyright © 2017 StayTuned. All rights reserved.
//

import UIKit
import Device

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIPopoverPresentationControllerDelegate {

    let kFileArray = "FileArray"
    
    var assets: NSArray = []
    var assetToShow: Dictionary = [String:String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (UserDefaults.standard.object(forKey: kFileArray) == nil) {
            let bundle = Bundle.main
            let path = bundle.path(forResource: "FileList", ofType: "plist")
            let array: NSArray = NSArray(contentsOfFile: path!)!
            UserDefaults.standard.set(array, forKey: kFileArray)
            self.assets = array
        } else {
            self.assets = UserDefaults.standard.array(forKey: kFileArray)! as NSArray
        }
        
        self.automaticallyAdjustsScrollViewInsets = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK: UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return assets.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:AssetTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AssetTableViewCell") as! AssetTableViewCell
        
        cell.setupCellWithDict(asset: assets.object(at: indexPath.row) as! [String : String])
        
        return cell
    }
    
    //MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let viewController: AssetViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AssetViewController") as! AssetViewController
        viewController.currentAsset = assets.object(at: indexPath.row) as! [String : String]
        
        self.show(viewController, sender: nil)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.frame.height / CGFloat(assets.count);
    }
    
    //MARK: IBActions 
    
    @IBAction func aboutButtonPressed(_ sender: UIBarButtonItem) {
        let viewController: UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AboutViewController")
        
        if Device.type() == .iPad {
            viewController.modalPresentationStyle = UIModalPresentationStyle.popover
            
            let popvc = viewController.popoverPresentationController
            popvc?.delegate = self
            popvc?.permittedArrowDirections = UIPopoverArrowDirection.any
            popvc?.barButtonItem = sender
            viewController.preferredContentSize = CGSize.init(width: 320, height: 470)
            
            self.present(viewController, animated: true, completion: nil)
        } else {
            self.show(viewController, sender: nil)
        }
        
    }
    
    //MARK: UIPopoverPresentationControllerDelegate
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
    
}

