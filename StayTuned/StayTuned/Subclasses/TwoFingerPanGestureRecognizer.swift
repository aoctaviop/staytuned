//
//  TwoFingerPanGestureRecognizer.swift
//  StayTuned
//
//  Created by Andrés on 5/12/17.
//  Copyright © 2017 StayTuned. All rights reserved.
//

import UIKit
import UIKit.UIGestureRecognizerSubclass

class TwoFingerPanGestureRecognizer: UIPanGestureRecognizer {

    let kVerticalPanThreshold: Float = 2.0
    var panRecognized = false
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent) {
        super.touchesBegan(touches, with: event)
        if self.numberOfTouches == 2 {
            self.panRecognized = true
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent) {
        super.touchesMoved(touches, with: event)
        
        state = self.state;
        
        if (state == .failed || state == .cancelled || state == .ended) {
            self.panRecognized = false
            return;
        }
        
        if (!self.panRecognized) {
            let translation = self.translation(in:self.view)
            
            if fabsf(Float(translation.y)) > kVerticalPanThreshold {
                self.panRecognized = true
            }
        }
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent) {
        super.touchesCancelled(touches, with: event)
        self.panRecognized = false
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent) {
        super.touchesEnded(touches, with: event)
        self.panRecognized = false
    }
    
}
