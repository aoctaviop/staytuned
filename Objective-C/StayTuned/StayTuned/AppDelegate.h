//
//  AppDelegate.h
//  StayTuned
//
//  Created by Andrés on 5/11/17.
//  Copyright © 2017 StayTuned. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

