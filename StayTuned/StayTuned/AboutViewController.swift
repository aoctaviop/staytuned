//
//  AboutViewController.swift
//  StayTuned
//
//  Created by Andrés on 8/2/17.
//  Copyright © 2017 StayTuned. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController, UITextViewDelegate {

    @IBOutlet weak var textField: UITextView!
    
    let supportEmail = "support@staytunedapps.com"
    let webURL = "https://danielmunoz.cr"
    let slimFontRegular = UIFont.systemFont(ofSize: 14.0, weight: UIFontWeightLight)
    let slimFontPetite = UIFont.systemFont(ofSize: 11.0, weight: UIFontWeightLight)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let initialText = "StayTuned uses music to generate impulses in the brain to accelerate the expansion of the consciousness.\n\n\nIt improves our empathy, communication and intuition skills.\n\n\nContact: \(self.supportEmail)\n\n\nDeveloped by " as NSString
        
        let attributedText = NSMutableAttributedString (string: initialText as String, attributes: [NSFontAttributeName: self.slimFontRegular])
        
        let link = NSAttributedString (string: "DM", attributes: [NSLinkAttributeName : URL (string: self.webURL)!, NSFontAttributeName: self.slimFontRegular])
        
        attributedText.append(link)
        
        let acknowledgement = NSAttributedString (string: "\nIcons by Madebyoliver", attributes: [NSFontAttributeName: self.slimFontPetite])
        
        attributedText.append(acknowledgement)
        
        self.textField.attributedText = attributedText
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UITextFieldDelegate

    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        return true
    }

}
