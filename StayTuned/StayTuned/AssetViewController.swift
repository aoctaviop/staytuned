//
//  AssetViewController.swift
//  StayTuned
//
//  Created by Andrés on 5/10/17.
//  Copyright © 2017 StayTuned. All rights reserved.
//

import UIKit
import MediaPlayer
import UICircularProgressRing
import HGCircularSlider
import Toaster

extension Double {
    /// Rounds the double to decimal places value
    func roundTo(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

class AssetViewController: UIViewController, AVAudioPlayerDelegate, UIGestureRecognizerDelegate {
    
    var currentAsset: Dictionary = [String:String]()
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var circularSlider: CircularSlider!
    @IBOutlet weak var positionLabel: UILabel!
    @IBOutlet weak var rewindButton: UIButton!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var stopButton: UIButton!
    
    @IBOutlet weak var waitingView: UIView!
    @IBOutlet weak var blurView: UIView!
    @IBOutlet weak var progressView: UICircularProgressRingView!
    @IBOutlet weak var captionLabel: UILabel!
    
    var audioPlayer: AVAudioPlayer?
    var timer: Timer?
    var seekTime: CGFloat = 0.0
    var twoFingerPanRecognizer: TwoFingerPanGestureRecognizer?
    var panValue: String?
    var lastValue: Double = -1.0
    
    var gradientView: KWGradientView?
    var layer: CAGradientLayer?
    var visualEffectView: VisualEffectView?
    
    var fileURL : URL?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = self.currentAsset["displayName"]
        self.titleLabel.text = self.currentAsset["title"]
        self.descriptionTextView.text = self.currentAsset["detail"]
        
        self.circularSlider.endPointValue = 0.0
        
        do {
            try AVAudioSession.sharedInstance().setActive(true)
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
        } catch let error as NSError {
            print("AVAudioSession Error: \(error.localizedDescription)")
        }
        
        circularSlider.addTarget(self, action: #selector(pause), for: .editingDidBegin)
        circularSlider.addTarget(self, action: #selector(resume), for: .editingDidEnd)
        
        UIApplication.shared.keyWindow?.addGestureRecognizer(self.twoFingerPanGestureRecognizer())
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(image: UIImage.init(imageLiteralResourceName: "info"), style: .done, target: self, action: #selector(showVolumeToast))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if UserDefaults.standard.url(forKey: self.currentAsset["serverName"]!) == nil {
            self.showWaitingView()
            self.downloadFile()
        } else {
            self.hideWaitingView()
            self.fileURL = UserDefaults.standard.url(forKey: self.currentAsset["serverName"]!)
            self.setupAudioPlayer()
            self.initPlaybackTimeViews()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.stop()
        
        do {
            try AVAudioSession.sharedInstance() .setActive(false)
        } catch let error as NSError {
            print("AVAudioSession Error: \(error.localizedDescription)")
        }
        
        self.timer?.invalidate()
        
        UIApplication.shared.keyWindow?.removeGestureRecognizer(self.twoFingerPanGestureRecognizer())
        
        FilesDownloader.shared().cancelDownloads()
    }
    
    // MARK: - Gesture Helpers
    
    func didPan(recognizer: UIPanGestureRecognizer) {
        if !(self.audioPlayer?.isPlaying)! {
            return
        }
        
        self.updateSlider()
        self.circularSlider.isUserInteractionEnabled = false
        
        let progressWithinSelfView: CGFloat = self.progressOfTouchLocation(location: recognizer.location(in: self.view), inView: self.view)
        //let viewHeightRatio: CGFloat = self.bounds.size.height / volumeOverlay.bounds.size.height;
        
        let currentValue = Double(progressWithinSelfView).roundTo(places: 2)
        
        if self.lastValue == -1.0 {
            self.lastValue = currentValue
        } else {
            let difference = currentValue - self.lastValue
            if abs(difference) >= 0.05 {
                self.lastValue = currentValue
                if difference < 0.0 {
                    var newVolume = (self.audioPlayer?.volume)! - 0.1
                    if newVolume < 0.0 {
                        newVolume = 0.0
                    }
                    self.audioPlayer?.volume = newVolume
                } else {
                    var newVolume = (self.audioPlayer?.volume)! + 0.1
                    if newVolume > 1.0 {
                        newVolume = 1.0
                    }
                    self.audioPlayer?.volume = newVolume
                }
            }
        }
        
        if (self.twoFingerPanRecognizer?.panRecognized)! {
            
            if self.visualEffectView == nil {
                self.visualEffectView = VisualEffectView(frame: (UIApplication.shared.keyWindow?.frame)!)
                self.visualEffectView?.colorTint = .white
                self.visualEffectView?.colorTintAlpha = 0.1
                self.visualEffectView?.blurRadius = 3
            }
            
            if self.gradientView == nil {
                self.gradientView = KWGradientView(frame: (UIApplication.shared.keyWindow?.frame)!)
                self.layer = self.gradientView?.addGradientLayerAlongYAxis(colors: [.black])
            } else {
                self.layer = self.gradientView?.addGradientLayerAlongYAxis(colors: [.black])
                self.gradientView?.animateGradient(self.layer!, to: self.colorsArrayForCurrentVolume())
            }
            
            UIApplication.shared.keyWindow?.addSubview(self.visualEffectView!)
            UIApplication.shared.keyWindow?.addSubview(self.gradientView!)
        } else {
            self.restartTimer()
            self.resetUI()
        }
    }
    
    func colorsArrayForCurrentVolume() -> Array<UIColor> {
        var result: Array<UIColor> = []
        
        for value in stride(from: 1.0, through: 0.0, by: -0.07){
            if value > Double((self.audioPlayer?.volume)!) {
                result.append(UIColor.black.withAlphaComponent(0.9))
            } else {
                result.append(UIColor.init(red: 20.0/255.0, green: 133.0/255.0, blue: 204.0/255.0, alpha: 0.9))
            }
        }
        
        if Double((self.audioPlayer?.volume)!) == 0.0 {
            result = [UIColor.black.withAlphaComponent(0.9), UIColor.black.withAlphaComponent(0.9)]
        }
        
        return result
    }
    
    func progressOfTouchLocation(location: CGPoint, inView:UIView) -> CGFloat {
        let size = view.frame.size;
        var percentage: Float = 0.0;
        
        
        if location.y >= size.height {
            percentage = 0.0;
        } else if location.y < 0.0 {
            percentage = 1.0;
        } else {
            percentage = Float(1.0) - Float(location.y / size.height);
        }
        
        return CGFloat(percentage);
    }
    
    func resetUI() {
        self.visualEffectView?.removeFromSuperview()
        self.gradientView?.removeFromSuperview()
        
        self.circularSlider.isUserInteractionEnabled = true
    }
    
    // MARK: - Class Functions
    
    func showVolumeToast() {
        let toast = Toast(text: "When playing an audio you can try swiping up and down with two fingers to adjust volume.", duration: Delay.long)
        ToastView.appearance().font = UIFont.systemFont(ofSize: 17.0, weight: UIFontWeightLight)
        ToastView.appearance().backgroundColor = UIColor.init(colorLiteralRed: 20.0/255.0, green: 133.0/255.0, blue: 204.0/255.0, alpha: 0.8)
        toast.show()
    }
    
    func twoFingerPanGestureRecognizer() -> TwoFingerPanGestureRecognizer {
        if self.twoFingerPanRecognizer == nil {
            self.twoFingerPanRecognizer = TwoFingerPanGestureRecognizer(target: self, action: #selector(didPan(recognizer:)))
            self.twoFingerPanRecognizer?.minimumNumberOfTouches = 2
            self.twoFingerPanRecognizer?.delegate = self
            self.twoFingerPanRecognizer?.cancelsTouchesInView = true
            self.twoFingerPanRecognizer?.delaysTouchesEnded = true
        }
        
        return self.twoFingerPanRecognizer!
    }
    
    func stringFromTimeInterval(interval: TimeInterval) -> String {
        
        let ti = NSInteger(interval)
        
        let seconds = ti % 60
        let minutes = (ti / 60) % 60
        let hours = (ti / 3600)
        
        var result = "00:00"
        
        if hours > 0 {
            result = String(format: "%0.2d:%0.2d:%0.2d", hours, minutes, seconds)
        } else if minutes > 0 {
            result = String(format: "%0.2d:%0.2d", minutes, seconds)
        } else if seconds > 0 {
            result = String(format: "00:%0.2d", seconds)
        }
        
        return result
    }
    
    func setupAudioPlayer() {
        do {
            try audioPlayer = AVAudioPlayer(contentsOf: self.fileURL!)
            audioPlayer?.volume = 0.5
            audioPlayer?.delegate = self
            audioPlayer?.prepareToPlay()
        } catch let error as NSError {
            print("audioPlayer error \(error.localizedDescription)")
        }
    }
    
    func initPlaybackTimeViews() {
        self.circularSlider.endPointValue = 0.0;
        self.circularSlider.maximumValue = CGFloat((self.audioPlayer?.duration)!);
        self.positionLabel?.text = "00:00"
    }
    
    func updateSlider() {
        self.circularSlider.endPointValue = CGFloat((self.audioPlayer?.currentTime)!)
        self.positionLabel.text = self.stringFromTimeInterval(interval: (self.audioPlayer?.currentTime)!)
    }
    
    func restartTimer() {
        self.timer?.invalidate()
        self.timer = Timer .scheduledTimer(timeInterval: 0.04, target: self, selector: #selector(updateSlider), userInfo: nil, repeats: true)
    }
    
    // MARK: - Playback Functions
    
    func stop() {
        self .initPlaybackTimeViews()
        self.audioPlayer?.stop()
        self.audioPlayer?.currentTime = 0.0
        self.playButton.setImage(UIImage (imageLiteralResourceName: "play"), for: UIControlState.normal)
        self.playButton.isSelected = false
        self.rewindButton.isEnabled = false
        self.stopButton.isEnabled = false
    }
    
    func pause() {
        self.timer?.invalidate()
    }
    
    func resume() {
        
        if (self.audioPlayer?.isPlaying)! {
            
            let seek = String.init(format: "%.2f", self.seekTime)
            let total = String.init(format: "%.2f", CGFloat((self.audioPlayer?.duration)!))
            let seekNumber = Float(seek)
            let totalNumber = Float(total)
            
            if (seekNumber?.isLess(than: totalNumber!))! {
                self.audioPlayer?.stop()
                self.audioPlayer?.currentTime = TimeInterval(self.seekTime)
                self.audioPlayer?.play()
                self.timer = Timer .scheduledTimer(timeInterval: 0.04, target: self, selector: #selector(updateSlider), userInfo: nil, repeats: true)
            } else {
                self.stop()
                self.circularSlider.endPointValue = 0.0
                self.circularSlider.isUserInteractionEnabled = false
            }
        }
    }
    
    // MARK: - IBAction
    
    @IBAction func circularSliderValueChanged(_ sender: CircularSlider) {
        self.seekTime = circularSlider.endPointValue
        self.positionLabel.text = self.stringFromTimeInterval(interval: TimeInterval(self.seekTime))
    }
    
    @IBAction func rewindButtonPressed(_ sender: UIButton) {
        if !(self.audioPlayer?.isPlaying)! {
            self.playButton.setImage(UIImage (imageLiteralResourceName: "pause"), for: UIControlState.normal)
        }
        
        self.audioPlayer?.stop()
        self.audioPlayer?.currentTime = 0.0
        self.audioPlayer?.play()
        
        self.restartTimer()
        
        self.circularSlider.isUserInteractionEnabled = true
    }
    
    @IBAction func playButtonPressed(_ sender: UIButton) {
        if !sender.isSelected {
            self.audioPlayer?.play()
            self.timer?.invalidate()
            self.timer = Timer .scheduledTimer(timeInterval: 0.04, target: self, selector: #selector(updateSlider), userInfo: nil, repeats: true)
            sender .setImage(UIImage (imageLiteralResourceName: "pause"), for: UIControlState.normal)
        } else {
            self.audioPlayer?.pause()
            self.timer?.invalidate()
            sender.setImage(UIImage (imageLiteralResourceName: "play"), for: UIControlState.normal)
        }
        
        self.rewindButton.isEnabled = true
        self.stopButton.isEnabled = true
        
        sender.isSelected = !sender.isSelected
        
        self.circularSlider.isUserInteractionEnabled = sender.isSelected
    }
    
    @IBAction func stopButtonPressed(_ sender: UIButton) {
        self.stop()
        self.circularSlider.isUserInteractionEnabled = false
    }
    
    // MARK: - AVAudioPlayerDelegate
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully
        flag: Bool) {
        self.resetUI()
        self.stop()
    }
    
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        self.resetUI()
    }
    
    func audioPlayerBeginInterruption(_ player: AVAudioPlayer) {
        if (self.audioPlayer?.isPlaying)! {
            self.audioPlayer?.pause()
        }
    }
    
    func audioPlayerEndInterruption(player: AVAudioPlayer) {
        self.audioPlayer?.play()
    }
    
    // MARK: - File Downloading Methods
    
    func showWaitingView() {
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.navigationItem.rightBarButtonItem?.isEnabled = false;
        
        let blurEffectView = VisualEffectView(frame: self.view.frame)
        blurEffectView.colorTint = .white
        blurEffectView.colorTintAlpha = 0.1
        blurEffectView.blurRadius = 3
        
        self.blurView.addSubview(blurEffectView)
        
        UIView.animate(withDuration: 0.3) {
            self.waitingView.alpha = 1.0
        }
    }
    
    func hideWaitingView() {
        self.navigationItem.setHidesBackButton(false, animated: true)
        self.navigationItem.rightBarButtonItem?.isEnabled = true;
        
        UIView.animate(withDuration: 0.3) {
            self.waitingView.alpha = 0.0
        }
    }
    
    func downloadFile() {
        self.showWaitingView()
        
        FilesDownloader.shared().downloadFile(fileName: self.currentAsset["serverName"]!, progressUpdate: { (progress: Double) in
            DispatchQueue.main.async {
                self.updateProgressValue(progress: progress)
            }
        }) { (fileURL: URL) in
            UserDefaults.standard.set(fileURL, forKey: self.currentAsset["serverName"]!)
            self.fileURL = fileURL;
            self.setupAudioPlayer()
            self.initPlaybackTimeViews()
            let when = DispatchTime.now() + 0.3
            DispatchQueue.main.asyncAfter(deadline: when) {
                self.hideWaitingView()
            }
        }
    }
    
    func updateProgressValue(progress: Double) {
        self.progressView.setProgress(value: CGFloat(progress * 100.0), animationDuration: 0.3)
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return gestureRecognizer.numberOfTouches < 2;
    }
}
